<!-- https://viblo.asia/p/validate-du-lieu-form-don-gian-hieu-qua-hon-voi-jquery-validation-XL6lAkwJKek -->
<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" type="text/css" href="./a.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
    <title style="text-align: center;">Form đăng ký sinh viên</title>
</head>


<body id="body">
    <p style="text-align: center;">Form đăng ký sinh viên</p>
    <p id="alert" style="padding-left: 2.5cm; margin-bottom : 0;color : red;"></p>
    <form action="regist_student.php" method="post" enctype="multipart/form-data">

        <div class="container">
            <fieldset class="group_name">
                <label id="name" for="uname">Họ và tên</label>
                <input id="name_input" type="text" name="uname">
            </fieldset>

            <?php
            $gender = array(0 => "Nam", 1 => "Nữ");
            $department = array(
                "nothing" => "Chọn phân khoa", "MAT" => "Khoa học máy tính",
                "KDL" => "Khoa học vật liệu"
            );
            ?>

            <fieldset class="group_gender">
                <label id="gender" for="gen">Giới tính</label>
                <?php
                // foreach($gender as $key => $value):
                // echo '<option value="'.$key.'">'.$value.'</option>'; //close your tags!!
                // endforeach;
                for ($x = 0; $x < 2; $x++) {
                    echo '<input class = "gender_option" type="radio" name="gen" value="' . $x . '"> ' . $gender[$x];
                }
                ?>
            </fieldset>



            <fieldset class="date_group">
                <label id="date" for="d">Ngày sinh</label>
                <?php
                date_default_timezone_set('Asia/Ho_Chi_Minh');
                $year_now = (int)date("Y");
                $day = array("no_day" => "");
                for ($i = 1; $i <= 31; $i++) {
                    $day[$i] = $i;
                }
                $thang = array("no_month" => "");
                for ($i = 1; $i <= 12; $i++) {
                    $thang[$i] = $i;
                }

                $nam = array("no_year" => "");
                for ($i = $year_now - 40; $i <= $year_now - 15; $i++) {
                    $nam[$i] = $i;
                }
                ?>
                Năm
                <select id="select_year" name="year">
                    <?php
                    foreach ($nam as $key => $value) :

                        echo '  <option value="' . $key . '">' . $value . '</option>'; //close your tags!!

                    endforeach;
                    ?>
                </select>
                Tháng
                <select id="select_month" name="month">
                    <?php
                    foreach ($thang as $key => $value) :

                        echo '<option value="' . $key . '">' . $value . '</option>'; //close your tags!!

                    endforeach;
                    ?>
                </select>

                Ngày
                <select id="select_day" name="day">
                    <?php
                    foreach ($day as $key => $value) :

                        echo '<option value="' . $key . '">' . $value . '</option>'; //close your tags!!

                    endforeach;
                    ?>
                </select>
                <!-- <input id="date_input" placeholder="Enter Username" data-date="" data-date-format="DD/MM/YYYY" type="date" name="d" max=<?php echo $day ?>> -->
            </fieldset>

            <fieldset style="color: black;" class="address_group">
                <label style="color: white;" id="add_label" for="add">Địa chỉ</label>
                <?php
                $thanh_pho = array("nothing" => "", "hn" => "Hà Nội", "hcm" => "Tp.Hồ Chí Minh");

                ?>

                Thành phố
                <select style="width: 150px;" id="select_city" name="city">
                    <?php
                    foreach ($thanh_pho as $key => $value) :

                        echo '<option value="' . $key . '">' . $value . '</option>'; //close your tags!!

                    endforeach;
                    ?>
                </select>
                Quận 
                <select style="width: 150px;" id="select_district" name="district">
                    <option value=""></option>
                </select>

                <!-- <textarea id="add_input" rows="5" cols="42" name="add" wrap="hard"></textarea> -->
            </fieldset>

            <fieldset class="image_group">
                <label id="img_label" for="fileToUpload">Thông tin khác</label>
                <textarea id="add_input" rows="5" cols="42" name="info" wrap="hard"></textarea>
                <!-- <input type="text" name="fileToUpload" id="fileToUpload" accept=".jpg, .png, .jpeg"> -->
            </fieldset>

            <fieldset class="register">
                <button id="my_button" name="button" type="submit">Đăng ký</button>

                <!-- <label>
      <input type="checkbox" checked="checked" name="remember"> Remember me
    </label> -->
            </fieldset>
        </div>

        <!-- <div class="container" style="background-color:#f1f1f1">
  <button type="button" class="cancelbtn">Cancel</button>
  <span class="psw">Forgot <a href="#">password?</a></span>
</div> -->
    </form>
    <!-- <p id="test">asdas</p> -->
    <script type="text/javascript" src="./a.js"></script>
</body>

</html>