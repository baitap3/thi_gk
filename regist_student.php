<!-- https://viblo.asia/p/validate-du-lieu-form-don-gian-hieu-qua-hon-voi-jquery-validation-XL6lAkwJKek -->
<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" type="text/css" href="./confirm.css">
    <style>
        label::after {
            margin-left: 5px;
            font-size: 20px;
            content: "";
            /* color: red; */
        }
    </style>
</head>


<body id="body">
    <?php


    // if ($_SERVER["REQUEST_METHOD"] == "POST") {
    //     // collect value of input field
    //     $name = $_POST;
    //     if (empty($name)) {
    //         echo "Name is empty";
    //     } else {
    //         foreach ($name as $key => $value) :
    //             echo "key= " . $key . " ; value = " . $value . "<br/>";
    //         endforeach;
    //     }
    // }

    // }
    ?>


    <div class="container">
        <fieldset class="group_name" style="color : black">
            <label id="name" for="uname" style="color: white;">Họ và tên</label>
            <?php
            echo $_POST["uname"];
            ?>
        </fieldset>

        <fieldset class="group_gender">
            <label id="gender" for="gen">Giới tính</label>
            <?php
            $gender = array(0 => "Nam", 1 => "Nữ");
            $department = array(
                "nothing" => "Chọn phân khoa", "MAT" => "Khoa học máy tính",
                "KDL" => "Khoa học vật liệu"
            );
            echo $gender[$_POST["gen"]];
            ?>
        </fieldset>



        <fieldset class="date_group">
            <label id="date" for="d">Ngày sinh</label>
            <?php
            echo $_POST["day"] . "/" . $_POST["month"] . "/" . $_POST["year"];
            ?>
            <!-- <input id="date_input" placeholder="Enter Username" data-date="" data-date-format="DD/MM/YYYY" type="date" name="d" max=<?php //echo $day 
                                                                                                                                            ?>> -->
        </fieldset>



        <fieldset class="address_group">
            <?php
            $thanh_pho = array("nothing" => "", "hn" => "Hà Nội", "hcm" => "Tp.Hồ Chí Minh");

            ?>
            <label id="add_label" for="add">Địa chỉ</label>
            <div id="addres_info">
                <?php
                echo $_POST["district"] . " - " .  $thanh_pho[$_POST["city"]];
                ?>
            </div>
            <!-- <input id="add_input" type="text" name="add"> -->

            <!-- <textarea id="add_input" rows="5" cols="42" name="add" wrap="hard"></textarea> -->
        </fieldset>

        <fieldset class="image_group" style="color: black;">
            <label  style="top:0cm; color:white;" id="img_label" for="fileToUpload">Thông tin khác</label>
            <!-- <img src= onerror="this.onerror=null; this.src='default.jpg'" width="200px" height="150px"> -->
            <?php
            echo $_POST["info"];
            ?>

            <!-- <input type="file" name="fileToUpload" id="fileToUpload" accept=".jpg, .png, .jpeg"> -->
        </fieldset>



    </div>

</body>

</html>