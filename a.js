var check = false;
var height = document.body.scrollHeight;

var thanhPhoSelect = document.getElementById("select_city");
var quanSelect = document.getElementById("select_district");


// let date = $("#date_input");
// if (!$("#date_input").val()) {
$("#date_input").on("change", function () {
    if (!$("#date_input").val()) {
        this.setAttribute(
            "data-date", "dd/mm/yyyy");
    } else {
        this.setAttribute(
            "data-date",
            moment(this.value, "YYYY-MM-DD")
                .format(this.getAttribute("data-date-format"))
        );
    }

}).trigger("change");


// }

const quanTheoThanhPho = {
    hn: ["Hoàng Mai", "Thanh Trì", "Nam Từ Niêm", "Hà Đông", "Cầu Giấy"],
    hcm: ["Quận 1", "Quận 2", "Quận 3","Quận 7", "Quận 9"]
};

// if ($("#date_input").val()) {
// $("#date_input").on("change", function () {
//     this.setAttribute(
//         "data-date",
//         moment(this.value, "YYYY-MM-DD")
//             .format(this.getAttribute("data-date-format"))
//     )
// }).trigger("change");
// }

$("#my_button").click(function () {
    check = true;
    let alert = "";
    let count = 0;
    let usernameValue = $("#name_input").val();
    let day = $("#select_day option:selected").val();
    let month = $("#select_month option:selected").val();
    let year = $("#select_year option:selected").val();
    let date = $("#select_city option:selected").val();
    if (usernameValue.length == "") {
        alert += "Hãy nhập họ tên.<br/>";
        count++;
    }

    // check a radio is choose or not
    if (!($('.gender_option:checked').length)) {
        alert += ('Hãy chọn giới tính.<br/>');
        count++;
    }

    if (day == "no_day" || month == "no_month" ||year == "no_year") {
        alert += "Hãy chọn ngày sinh.<br/>";
        count++;
    }

    if (date == "nothing") {
        alert += "Hãy chọn địa chỉ.<br/>";
        count++;
    }

    if (count == 0) {
        return true;
    } else {
        document.getElementById("alert").innerHTML = alert;
        document.body.style.height = String(height + 16 * count) + "px";
        
        return false;
    }
});

thanhPhoSelect.addEventListener("change", function() {
    // Xóa tất cả các lựa chọn cũ trong select quận
    while (quanSelect.firstChild) {
        quanSelect.removeChild(quanSelect.firstChild);
    }

    // Lấy giá trị của thành phố đã chọn
    const selectedThanhPho = thanhPhoSelect.value;

    // Lặp qua danh sách quận và thêm từng quận vào select quận
    quanTheoThanhPho[selectedThanhPho].forEach(function(quan) {
        const option = document.createElement("option");
        option.value = quan;
        option.text = quan;
        quanSelect.appendChild(option);
    });
});

thanhPhoSelect.dispatchEvent(new Event("change"));



// $("#my_button").click(function () {
//     check = true;
//     if (check) {
//         $("form").validate({
//             rules: {
//                 name_input: "required",
//                 date_input: "required"
//             },

//             messages: {
//                 name_input: "Please enter your firstname",
//                 date_input: "Please enter your firstname",
//             },

//             submitHandler: function (form) {
//                 form.submit();
//             },
//             errorPlacement: function (error, element) {
//                 if (check) {
//                     let alert = "";
//                     let count = 0;
//                     if (element.attr('name') == 'uname') {
//                         // error.insertAfter('#alert');
//                         alert += "Hãy nhập tên.<br/>";
//                         count++;

//                     }
//                     if (element.attr('name') == 'gen') {
//                         alert += "Hãy chọn giới tính.<br/>";
//                         count++;
//                     }
//                     if (element.attr('name') == 'depart') {
//                         alert += "Hãy chọn khoa.<br/>";
//                         count++;
//                     }
//                     if (element.attr('name') == 'd') {
//                         alert += "Hãy nhập ngày sinh.<br/>";
//                         count++;
//                     }

//                     document.getElementById("alert").innerHTML = alert;
//                     document.body.style.height = String(height + 32) + "px";
//                 }
//                 check = false;
//             }
//         });
//     }

//     // document.getElementById("test").innerHTML = document.getElementById("my_button").getBoundingClientRect().top
//     //  + ":" + document.getElementById("alert").offsetHeight;
// });